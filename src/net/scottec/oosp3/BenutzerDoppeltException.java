package net.scottec.oosp3;

/**
 * Exception
 * Kennzeichnet Dopplung in Datenhaltung
 */
public class BenutzerDoppeltException extends Exception {
    public BenutzerDoppeltException(String message) {
        super(message);
    }
}
