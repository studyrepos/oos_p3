package net.scottec.oosp3;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class BenutzerVerwaltungAdminTest {

    private BenutzerVerwaltungAdmin admin;

    @Test
    public void benutzerEintragenSuccess() {
        admin = new BenutzerVerwaltungAdmin("test.s", true);
        Benutzer a = new Benutzer("NutzerA", "abc".toCharArray());
        Benutzer b = new Benutzer("NutzerB", "123".toCharArray());

        try {
            admin.benutzerEintragen(a);
            admin.benutzerEintragen(b);

            assertTrue(admin.benutzerOk(a));
            assertTrue(admin.benutzerOk(b));
        }
        catch (BenutzerDoppeltException exp) {
            fail("Should have worked...");
        }
    }

    @Test
    public void benutzerEintragenFail() {
        admin = new BenutzerVerwaltungAdmin("test.s", true);
        Benutzer a = new Benutzer("NutzerA", "abc".toCharArray());

        try {
            admin.benutzerEintragen(a);
            assertTrue(admin.benutzerOk(a));
            admin.benutzerEintragen(a);
            fail("Expected BenutzerDoppeltException to be thrown");
        }
        catch (BenutzerDoppeltException exp) {
            assertThat(exp.getMessage(), is("Benutzer bereits vorhanden"));
        }
    }


    @Test
    public void benutzerOkSuccess() {
        admin = new BenutzerVerwaltungAdmin("test.s", true);
        Benutzer a = new Benutzer("NutzerA", "abc".toCharArray());

        try {
            admin.benutzerEintragen(a);
        }
        catch (BenutzerDoppeltException exp) {
            exp.printStackTrace();
        }

        assertEquals(true, admin.benutzerOk(a));
    }

    @Test
    public void benutzerOkFail() {
        admin = new BenutzerVerwaltungAdmin("test.s", true);

        assertEquals(false, admin.benutzerOk(new Benutzer("BenutzerC", "321".toCharArray())));
    }


    @Test
    public void benutzerLoeschenSuccess() throws Exception{
        admin = new BenutzerVerwaltungAdmin("test.s", true);
        Benutzer a = new Benutzer("NutzerA", "abc".toCharArray());
        admin.benutzerEintragen(a);
        assertTrue(admin.benutzerOk(a));

        try {
            admin.benutzerLoeschen(a);
            assertFalse(admin.benutzerOk(a));
        }
        catch (BenutzerNichtGefundenException exp) {
            fail("Should have worked...");
        }
    }

    @Test
    public void benutzerLoeschenFail() throws Exception{
        admin = new BenutzerVerwaltungAdmin("test.s", true);
        Benutzer a = new Benutzer("NutzerA", "abc".toCharArray());

        try {
            admin.benutzerLoeschen(a);
            fail("Expected BenutzerNichtGefundenException");
        }
        catch (BenutzerNichtGefundenException exp) {
            assertThat(exp.getMessage(), is("Benutzer nicht gefunden"));
        }
    }

    @Test
    public void mehrereAdmins() {
        BenutzerVerwaltungAdmin admin1 = new BenutzerVerwaltungAdmin("test1.s", false);
        BenutzerVerwaltungAdmin admin2 = new BenutzerVerwaltungAdmin("test1.s", false);

        Benutzer a = new Benutzer("NutzerXY", "pass".toCharArray());

        try {
            admin1.benutzerEintragen(a);
            assertTrue(admin2.benutzerOk(a));
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
