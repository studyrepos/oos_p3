package net.scottec.oosp3;

import org.junit.Test;

import static org.junit.Assert.*;

public class BenutzerTest {
    @Test
    public void equalsBenutzerSuccess() throws Exception {
        Benutzer a = new Benutzer("NutzerA", "abc".toCharArray());
        Benutzer c = new Benutzer("NutzerA", "abc".toCharArray());

        assertTrue(a.equals(c));
    }

    @Test
    public void equalsBenutzerFail() throws Exception {
        Benutzer a = new Benutzer("NutzerA", "abc".toCharArray());
        Benutzer b = new Benutzer("NutzerB", "123".toCharArray());

        assertFalse(a.equals(b));
    }
}